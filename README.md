after `docker-compose up --build -d` and `composer install`(in /var/pyrospy directory)

check phpspy works:
1. inside 'phpspy-fpm' container via `docker exec -it phpspy-fpm sh` start phpspy:
```
phpspy --max-depth=-1 -V82 --time-limit-ms=5000 --threads=1024 --buffer-size=65536 -P fpm >trace.log 2> error.log 
```
1. start any script (f.e. by visit localhost)
1. look at result at `trace.log` file

check phpspy can send logs to pyroscpope server:
1. inside 'phpspy-fpm' container via `docker exec -it phpspy-fpm sh` start phpspy with sender-script:
1. 
```
phpspy --max-depth=-1 -V82 --time-limit-ms=5000 --threads=1024 --buffer-size=65536 -P fpm 2> error.log | php ./../pyrospy/pyrospy.php run --pyroscope=http://pyroscope:4040 --rateHz=99 --app=testApp --tags=host=server234 --tags=role=web
```
1. start any script (f.e. by visit localhost)
1. look result exist at pyroscope server at `localhost:4040`