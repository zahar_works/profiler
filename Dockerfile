FROM php:8.2.4-fpm-alpine

RUN apk add --update alpine-sdk \
    && git clone --recursive https://github.com/adsr/phpspy.git \
    && cd phpspy && make && mv ./phpspy /usr/bin/phpspy

# install composer 2.5.1
COPY ./composer_2-5-1.phar /usr/bin/composer
RUN chmod 777 /usr/bin/composer

RUN adduser -S -s /bin/bash -u 1000 -D -h /home/appuser appuser
RUN sed -i -e 's/^user =.*/user = appuser/' /usr/local/etc/php-fpm.d/www.conf